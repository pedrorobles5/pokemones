




@extends('plantilla')
@section('titulo') 
- Listado
    
@endsection
@section('principal')
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Habilidades</th>
                                <th>Pokedex</th>
                                <th>Editar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pokemones as $i => $row )
                            <tr>
                                <td>{{ ($i+1) }}</td>
                                <td>{{ $row->nombre}}</td>
                                <td>{{ $row->tipo}}</td>
                                <td>{{ $row->habilidades}}</td>
                                <td>{{ $row->pokedex}}</td>
                                <td>
                                    <a class="btn btn-warning" href="{{ route('pokemones.edit',$row->id)}}">
                                        Editar
                                    </a>
                                    
                                </td>
                                <td>
                                    <form method="POST" action="{{route('pokemones.destroy',$row->id)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger">Eliminar </button>

                                    </form>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection