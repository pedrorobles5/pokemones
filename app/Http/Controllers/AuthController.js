





import Jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs/dist/bcrypt'
import mongoose from 'mongoose'
import { JWT_EXPIRES,JWT_SECRET } from '../../../resources/js/config'

const esquema = new mongoose.Schema({
    nombre:String,correo:String,passwword:String
},{versionKey:false})

const UsuarioModel = new mongoose.model('usuarios',esquema)

export const crearUsuario = async(req,res)=>{
    try{
        const {nombre,correo,passwword} = req.body
        var validacion = validar(nombre,correo,passwword)
        if (validacion == ''){
            let pass = await bcrypt.hash(passwword,8)
        const nuevoUsuario = new UsuarioModel({
            nombre:nombre,correo:correo,passwword:passwword
        })
        await nuevoUsuario.save()
        return res.status(200).json({status:true,message:'Usuario creado'})
    }else{
        return res.status(400).json({status:false,message:validacion})
    } 
} catch (error){
    return res.status(500).json({status:false,message:[error.message]})
}
}
    


const validar = (nombre,correo,passwword) =>{

    var errors = []
    if(nombre === undefined || nombre.trim() === ''){
        errors.push('El nombre No debe estar vacio')
    }
    if(correo === undefined || nombre.trim() === ''){
        errors.push('El correo No debe estar vacio')
    }
    if(passwword === undefined || nombre.trim() === '' || passwword.length < 8){
        errors.push('El contrase;a No debe estar vacio')
    }
    return errors

}